README

This file describes the several steps to follow in order to launch the demo to create a 100GE service using a request built in ONAP and send to the external controller TPCE.

Pre-requisite
The platform Open-Roadm is embedeed in a virtual Machine on the ONAP OpenLab Platform from Lannion.
The platform Open-Roadm should be up and running. The platform is based on virtual OpenRoadm optical Nodes (Honey Nodes). If the VM is up and running, it includes automatically a set of internal links between the nodes, which would allow to play this demo 100GE (only this one).

Step 1) "The Service creation Recipe" 
The bpmn file is the recipe to create the 100GE service.
Download the last version from Txx.bpmn file on the Gitlab
Copy this file on the OpenLab instance


Step 2bis) "Add your personnal touch"
Two objects should be created manually in the AAI Datastore from ONAP before the Recipe execution. 
Download the last version from Curl_Commands_MDONS_100GE file on the Gitlab
Launch the curl commands, adapted with current ONAP settings to create those two objects.


Step 3) "Get into the kitchen"
Once the bpmn is copied, user should "deploy" this recipe, as if you were programming your "Kitchenaid robot".
To do so customize and launch the following command with current ONAP settings:

curl -v -w -H "Accept: application/json" -F "deployment-name=rest-test2" -F "enable-duplicate-filteringpmn=@/home/debian/Txx.bpmn" http://so.api.simpledemo.onap.org:30407/sobpmnengine/deployment/create 

Txx is the name of the bpmn you downlaod previously.
/home/debian should be changed using your directory info
address and port from ONAP might also be adapted.

Step 4) "Open your eyes"
Launch the Log to check execution:

Execute the pod associated to the so-infra. 
To do so customize and launch the following command with current ONAP settings:

kubectl -n onap exec -it onap-so-so-bpmn-infra-56496b44db-c9vmz sh

Wait for App# prompt to come, then launch the command:

tail -f /app/logs/bpmn/debug.log

Step 5) "Execute the Recipe"
To create a service, a bpmn request is needed to be launched by the ONAP API handler. 
Download the demo_body_testTPCE4 file on the Gitlab.
Copy this file on the OpenLab instance.
The body from the request for this demo is the demo_body_testTPCE4 file.It gets all parameters (external and internal to ONAP) associated to the service 100GE user would create.
Execute the service creation taking into account all the parameters by the command (To do so customize and launch the following command with current ONAP settings):
  
curl -v -X POST   http://so.api.simpledemo.onap.org:30277/onap/so/infra/serviceInstantiation/v7/serviceInstances/   -H 'Accept: application/jasic SW5mcmFQb3J0YWxDbGllbnQ6cGFzc3dvcmQxJA=='   -H 'Content-Type: application/json'   -H 'X-ONAP-PartnerName: NBI'   -H 'cache-control: no-cache'   --data-binary @"/home/debian/demo_body_testTPCE4" 

address and port from ONAP might also be adapted.

Step 6) "Dinner is ready !!"
Verify in the Log that execution is well performed.

Step 7) "Enjoy your meal"

 
